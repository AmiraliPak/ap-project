﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Windows.Xps.Serialization;

namespace AP_Final_Project
{
    [Serializable]
    public class Restaurant 
    {
        public static void Save() { Extensions.Serialize("Restaurant.bin", restaurant); }
        public static Restaurant restaurant;

        public Restaurant(string District,string Kind, string MenuImage_Path, string Address, string PhoneNumber, List<Food> Menu,List<Order> Orders)
        {
            this.District = District;
            this.Kind = Kind;
            this.MenuImage_Path = MenuImage_Path;
            this.Address = Address;
            this.PhoneNumber = PhoneNumber;
            this.Menu = Menu;
            this.Orders = Orders;

            restaurant = this;
            Extensions.Serialize("Restaurant.bin", this);
        }
        public Restaurant() : this("Narmak", "FastFood", null, "Heydarkhani, Elm O Sanat", "02177123456", new List<Food>(), new List<Order>()) { }
        public string District { get; set; }

        public string Kind { get; set; }

        public string MenuImage_Path { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public List<Food> Menu { get; private set; }

        public List<Order> Orders { get; set; }

        public void AddToMenu(FoodKind foodKind, string Title, string Ingredients, long CostPrice, string ExtraInfo = null, string Image_Path = null)
        {
            if (Menu.Select(x => x.Title).Contains(Title))
                throw new Exception("This food title is already in menu.");
            Menu.Add(new Food(foodKind, Title, Ingredients, CostPrice, ExtraInfo, Image_Path));
            Save();
        }
    }
}