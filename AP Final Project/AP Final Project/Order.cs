﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AP_Final_Project
{
    [Serializable]
    public class Order
    {
        public Order(Customer customer,DateTime OrderDueDate, DiscountCode discountCode,Canvas canvas)
        {
            var Cart = customer.Cart;

            if (Cart.Count == 0)
                throw new Exception("Cart is empty");

            this.customer = customer;
            TaxPercentage = customer.TaxPercentage;
            DiscountPercentage_NoCode = customer.DiscountPercentage;

            //discount code
            this.discountCode = discountCode;
            if(discountCode!=null)
                discountCode.Used = true;

            this.OrderDueDate = OrderDueDate;
            OrderDateTime = DateTime.Now;

            //ordered food and decrease menu food quantity
            OrderedFood = new List<OrderedFood>(Cart);
            foreach(var orderedFood in OrderedFood)
                orderedFood.InMenu.DateQuantity[OrderDueDate] -= orderedFood.Quantity;

            //order id
            OrderID = LastOrderID + 1;

            IsPaid = false;

            //signature file name
            string filename = $"E:/UNI/Term2/AP Project/ap-project/AP Final Project/AP Final Project/assets/{OrderID}.png";
            Signature_Path = filename;
            //signature image save
            Rect bounds = VisualTreeHelper.GetDescendantBounds(canvas);
            double dpi = 96d;
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)bounds.Width, (int)bounds.Height, dpi, dpi, System.Windows.Media.PixelFormats.Default);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(canvas);
                dc.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
            }
            rtb.Render(dv);
            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));
            try
            {
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                pngEncoder.Save(ms);
                ms.Close();
                System.IO.File.WriteAllBytes(filename, ms.ToArray());
            }
            catch (Exception err)
            {
                System.Windows.MessageBox.Show(err.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            Restaurant.restaurant.Orders.Add(this);
            Restaurant.Save();

            customer.Orders.Add(this);
            Customer.Save();
        }
        //properties
        public Customer customer;
        public string CustomerName { get=> customer.Name + " " + customer.LastName; }
        public int OrderID { get; }
        public static int LastOrderID
        {
            get
            {
                if (Restaurant.restaurant.Orders.Count == 0)
                    return 0;
                return Restaurant.restaurant.Orders.Last().OrderID;
            }
        }
        public DateTime OrderDateTime { get; private set; }
        public DateTime OrderDueDate { get; private set; }
        public List<OrderedFood> OrderedFood { get; private set; }
        public string Signature_Path { private set; get; }
        public bool IsCanceledByManager { get; set; }
        //prices
        public long TotalSellingPrice { get => OrderedFood.Sum(x => x.SellingPrice * x.Quantity); }
        public long TotalCostPrice { get => OrderedFood.Sum(x => x.CostPrice * x.Quantity); }
        public long Total_AfterTaxDiscount
        {
            get=>(100 + TaxPercentage - DiscountPercentage_NoCode - DiscountCode_Percentage()) * TotalSellingPrice / 100;
        }
        public int TaxPercentage { private set; get; }
        public int DiscountPercentage_NoCode { private set; get; }
        public bool IsPaid { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public DiscountCode discountCode { get; }
        private int DiscountCode_Percentage()
        {
            if (discountCode == null)
                return 0;

            return discountCode.Percentage;
        }
        //methods
        public string Print(bool ForManager)
        {
            string result = "";

            result += $"OrderID: {OrderID}\n";
            result += $"CustomerName: {CustomerName}\n";
            result += $"OrderDateTime: {OrderDateTime}\n";
            result += $"OrderDueDate: {OrderDueDate.ToString().Split(' ')[0]}\n";
            result += $"PaymentMethod: {PaymentMethod}  IsPaid: {IsPaid}\n";

            result += "===================\n";
            foreach(var orderedFood in OrderedFood)
                result += $"{orderedFood.Title} x{orderedFood.Quantity} ==> {orderedFood.Quantity * orderedFood.SellingPrice}\n";
            result += "===================\n";
            
            if(discountCode!=null)
                result += $"DiscountCode: {discountCode.Code}\n";

            result += $"UserDiscount: {DiscountPercentage_NoCode}%\n";
            result += $"Overall Discount: {DiscountPercentage_NoCode+DiscountCode_Percentage()}%\n";
            result += $"Tax: {TaxPercentage}%\n";
            result += $"TotalSelling: {TotalSellingPrice}\n";
            result += $"==>After Tax & Discount: {Total_AfterTaxDiscount}\n";

            if(ForManager)
                result += $"Total Profit: {Total_AfterTaxDiscount-TotalCostPrice}\n";

            return result;
        }
        public void Cancel()
        {
            foreach (var orderedFood in OrderedFood)
                orderedFood.InMenu.DateQuantity[OrderDueDate] += orderedFood.Quantity;

            //removing order from arrays
            if (!customer.Orders.Remove(this))
                throw new Exception("Error in removing order");
            if (!Restaurant.restaurant.Orders.Remove(this))
                throw new Exception("Error in removing order");

            discountCode.Used = false;

            Customer.Save();
            Restaurant.Save();
        }
    }

    public enum PaymentMethod
    {
        Online,
        Cash_WhenDelivered
    }
}