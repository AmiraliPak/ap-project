﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AP_Final_Project
{
    /// <summary>
    /// Interaction logic for CustomerMenu.xaml
    /// </summary>
    public partial class CustomerMenu : Window
    {
        bool InOrderingProcess = false;
        Customer customer;
        public CustomerMenu(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
            UserInfo.Text = customer.Name + "\n" + customer.LastName;

            //cancel orders by manager
            var CanceledOrders = customer.Orders.Where(x => x.IsCanceledByManager);
            if (CanceledOrders != null)
            {
                foreach (var order in CanceledOrders)
                {
                    if(order.IsPaid)
                        System.Windows.MessageBox.Show($"The order (ID: {order.OrderID}) has been canceled by the manager.\n{order.Total_AfterTaxDiscount} is transfered back to your account.", "", MessageBoxButton.OK, MessageBoxImage.Information);
                    else
                        System.Windows.MessageBox.Show($"The order (ID: {order.OrderID}) has been canceled by the manager.", "", MessageBoxButton.OK, MessageBoxImage.Information);
                    
                    order.Cancel();
                }
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (InOrderingProcess)
                return;

            var DefaultMessageColor = Brushes.AliceBlue;
            //when its off
            if (((Button)sender).BorderBrush != Brushes.Yellow)
            {
                //turn off other buttons
                Menu1.BorderBrush = Brushes.DeepSkyBlue;
                Menu2.BorderBrush = Brushes.DeepSkyBlue;
                Menu3.BorderBrush = Brushes.DeepSkyBlue;
                Menu4.BorderBrush = Brushes.DeepSkyBlue;

                //turn on this button
                ((Button)sender).BorderBrush = Brushes.Yellow;
                //set textblocks
                if (((Button)sender).Name == Menu1.Name)
                {
                    MessageBox.Foreground = DefaultMessageColor;
                    MessageBox.Text = "Profile info is shown above";
                    Desk.SelectedIndex = 0;
                    //profile image
                    Profile_Image.Source = new ImageSourceConverter().ConvertFromString(customer.ProfilePicture_Path) as ImageSource;
                    //preveous infos
                    Edit_Name.Text = customer.Name;
                    Edit_LastName.Text = customer.LastName;
                    Edit_Email.Text = customer.Email;
                    Edit_PhoneNumber.Text = customer.PhoneNumber;
                    Edit_Address.Text = customer.Address;

                    Edit_Password.Password = "";
                    Edit_Password2.Password = "";
                }
                else if (((Button)sender).Name == Menu2.Name)
                {
                    MessageBox.Foreground = DefaultMessageColor;
                    MessageBox.Text = "select a date and select food to add it to cart";
                    Desk.SelectedIndex = 1;

                    //DatePicker setup
                    //DatePicker.SelectedDate = null;
                    DatePicker.DisplayDateStart = DateTime.Today.Date;
                    DatePicker.DisplayDateEnd = DateTime.Today.Date.AddMonths(1);
                    DatePicker.IsEnabled = false;
                    //empty datagrid
                    AvailableFood_Table.SelectedItem = null;
                    if (DatePicker.SelectedDate == null)
                        AvailableFood_Table.Items.Clear();
                    //empty textblocks
                    SelectedFood_Title.Text = "";
                    quantity.Value = 0;
                    quantity.IsEnabled = false;

                    AddToCart.IsEnabled = false;
                    //empty food info
                    Food_Image.Source = null;
                    TitleInfo.Text = "";
                    FoodKindInfo.Text = "";
                    IngredientsInfo.Text = "";
                    ExtraInfoInfo.Text = "";
                    PriceInfo.Text = "";

                    //searchbox
                    textBox.Text = "";
                    SearchBox_TextChanged(textBox, null);
                }
                else if (((Button)sender).Name == Menu3.Name)
                {
                    try
                    {
                        MessageBox.Foreground = DefaultMessageColor;
                        MessageBox.Text = "Select a food to set quantity or remove from cart";
                        Desk.SelectedIndex = 2;

                        //show selected date
                        if (DatePicker.SelectedDate == null)
                        {
                            Desk.Visibility = Visibility.Collapsed;
                            throw new Exception("Select date from AddToCart page");
                        }
                        SelectedDate_Textblock.Text = DatePicker.SelectedDate.ToString().Split(' ')[0];

                        //datagrid
                        Cart_Table.SelectedItem = null;
                        Cart_Table.Items.Clear();
                        foreach (var orderedFood in customer.Cart)
                            Cart_Table.Items.Add(orderedFood);

                        //empty textblocks
                        SelectedCartFood_Title.Text = "";
                        quantity2.Value = 0;
                        quantity2.IsEnabled = false;

                        EditQuantity_Button.IsEnabled = false;
                        RemoveFood_Button.IsEnabled = false;
                    }
                    catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; return; }
                }
                else if (((Button)sender).Name == Menu4.Name)
                {
                    MessageBox.Foreground = DefaultMessageColor;
                    MessageBox.Text = "Select an order to show details";
                    Desk.SelectedIndex = 3;

                    if (customer.Orders == null)
                        customer.Orders = new List<Order>();
                    Customer.Save();

                    Orders_Table.SelectedItem = null;
                    Orders_Table.Items.Clear();
                    foreach (var Order in customer.Orders)
                        Orders_Table.Items.Add(Order);
                    
                    OrderPrint.Text = "";
                    Signature_Image.Source = null;
                    CancelOrder_Button.IsEnabled = false;
                }
                Desk.Visibility = Visibility.Visible;
            }
            //when its on
            else
            {
                MessageBox.Foreground = DefaultMessageColor;
                MessageBox.Text = "Select From Menu";
                ((Button)sender).BorderBrush = Brushes.DeepSkyBlue;
                Desk.Visibility = Visibility.Collapsed;
            }

        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Manager.Save();
            Restaurant.Save();
            Customer.Save();

            if (InOrderingProcess)
                return;
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }
        private void EditMenu_Table_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            return;
        }

        //    tab1   ///////////////////////////////////////
        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Edit_Name.Text != customer.Name)
                {
                    if (!Edit_Name.Text.IsValidName())
                        throw new Exception("Invalid Name");
                    customer.Name = Edit_Name.Text;
                }

                if (Edit_LastName.Text != customer.LastName)
                {
                    if (!Edit_LastName.Text.IsValidName())
                        throw new Exception("Invalid LastName");
                    customer.LastName = Edit_LastName.Text;
                }

                if (Edit_Email.Text != customer.Email)
                {
                    if (!Edit_Email.Text.IsEmail())
                        throw new Exception("Invalid Email");
                    customer.Email = Edit_Email.Text;
                }

                if (Edit_PhoneNumber.Text != customer.PhoneNumber && Edit_PhoneNumber.Text != "")
                    customer.PhoneNumber = Edit_PhoneNumber.Text;

                if(Edit_Password.Password != "" || Edit_Password2.Password != "")
                {
                    if (Edit_Password.Password != Edit_Password2.Password)
                        throw new Exception("Passwords do not match");
                    customer.ChangePassword(Edit_Password.Password);
                }
                Customer.Save();

                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Fields Successfully Changed";
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }

        }
        private void SetProfilePicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.jpg;*.png)|*.jpg;*.png";
            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                customer.ProfilePicture_Path = openFileDialog.FileName;
                Profile_Image.Source = new ImageSourceConverter().ConvertFromString(customer.ProfilePicture_Path) as ImageSource;
                Customer.Save();
                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Profile Image Successfully Changed";
            }
        }

        //   tab2     ///////////////////////////////////////
        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //empty cart
                customer.EmptyCart();

                if (DatePicker.SelectedDate == null)
                {
                    //empty datagrid
                    AvailableFood_Table.SelectedItem = null;
                    AvailableFood_Table.Items.Clear();
                    AvailableFood_Table.IsEnabled = false;
                    //empty textblocks
                    SelectedFood_Title.Text = "";
                    quantity.Value = 0;
                    quantity.IsEnabled = false;

                    AddToCart.IsEnabled = false;
                    //empty food info
                    Food_Image.Source = null;
                    TitleInfo.Text = "";
                    FoodKindInfo.Text = "";
                    IngredientsInfo.Text = "";
                    ExtraInfoInfo.Text = "";
                    PriceInfo.Text = "";

                    return;
                }
                var SelectedDate = DatePicker.SelectedDate ?? DateTime.Today.Date;

                var AvailableFood =
                    Restaurant.restaurant.Menu
                    .Where(x => x.DateQuantity.ContainsKey(SelectedDate) && x.DateQuantity[SelectedDate] > 0);

                if (AvailableFood.Count() == 0)
                    throw new Exception("No food available in the selected date");

                AvailableFood_Table.SelectedItem = null;//func
                AvailableFood_Table.Items.Clear();
                foreach (var food in AvailableFood)
                {
                    food.TempQuantity = food.DateQuantity[SelectedDate];

                    AvailableFood_Table.Items.Add(food);
                }

                AvailableFood_Table_SelectionChanged(null, null);
                AvailableFood_Table.IsEnabled = true;

                MessageBox.Foreground = Brushes.AliceBlue;
                MessageBox.Text = "Select a food to see information and add it to cart";

                DatePicker.IsEnabled = false;
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }
        private void AvailableFood_Table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AvailableFood_Table.SelectedItem == null)
            {
                //empty textblocks
                SelectedFood_Title.Text = "";
                quantity.Value = 0;
                quantity.IsEnabled = false;

                AddToCart.IsEnabled = false;
                //empty food info
                Food_Image.Source = null;
                TitleInfo.Text = "";
                FoodKindInfo.Text = "";
                IngredientsInfo.Text = "";
                ExtraInfoInfo.Text = "";
                PriceInfo.Text = "";
            }
            else
            {
                Food SelectedAvailableFood = (Food)AvailableFood_Table.SelectedItem;

                SelectedFood_Title.Text = SelectedAvailableFood.Title;
                quantity.Value = 1;
                quantity.Maximum = SelectedAvailableFood.TempQuantity;
                quantity.IsEnabled = true;

                AddToCart.IsEnabled = true;

                //show info
                Food_Image.Source = new ImageSourceConverter().ConvertFromString(SelectedAvailableFood.Image_Path) as ImageSource;
                TitleInfo.Text = SelectedAvailableFood.Title;
                FoodKindInfo.Text = SelectedAvailableFood.foodKind.ToString();
                IngredientsInfo.Text = SelectedAvailableFood.Ingredients;
                ExtraInfoInfo.Text = SelectedAvailableFood.ExtraInfo;
                PriceInfo.Text = SelectedAvailableFood.SellingPrice.ToString();
            }
        }
        private void ChangeDate_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = System.Windows.MessageBox.Show("Changing the date will empty your cart", "", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
                DatePicker.IsEnabled = true;
            else if (result == MessageBoxResult.Cancel)
                DatePicker.IsEnabled = false;
        }
        private void AddToCart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DatePicker.SelectedDate == null)
                    throw new Exception("Select a date");
                if (AvailableFood_Table.SelectedItem == null)
                    throw new Exception("Select a food from menu");

                Food SelectedAvailableFood = (Food)AvailableFood_Table.SelectedItem;
                var SelectedDate = DatePicker.SelectedDate ?? DateTime.Today.Date;

                if (quantity.Value <= 0 || quantity.Value>SelectedAvailableFood.TempQuantity)
                    throw new Exception("Wrong Quantity");

                customer.AddToCart(SelectedAvailableFood, (int)quantity.Value);
                Customer.Save();

                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Food was successfully added to cart";

                AvailableFood_Table.SelectedItem = null;
                AvailableFood_Table_SelectionChanged(null, null);
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }
        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool found = false;
            var border = SearchResult_Table.Parent as Border;

            string query = (sender as TextBox).Text;

            if (query.Length == 0)
            {
                // Clear   
                SearchResult_Table.Items.Clear();
                border.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                border.Visibility = System.Windows.Visibility.Visible;
            }

            // Clear the list 
            SearchResult_Table.SelectedItem = null;
            SearchResult_Table.Items.Clear();

            // Add the result 
            var MatchingFoodFromMenu =
                Restaurant.restaurant.Menu.Where(x => (x.Title + x.Ingredients + x.SellingPrice.ToString()).ToLower().Contains(query.ToLower()));
            
            foreach (var food in MatchingFoodFromMenu)
            {
                // The word starts with this... Autocomplete must work   
                SearchResult_Table.Items.Add(food);
                found = true;
            }

            if (!found)
            {
                border.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void SearchResult_Table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (SearchResult_Table.SelectedItem == null)
                    return;

                Food SelectedSearched = (Food)SearchResult_Table.SelectedItem;

                if (!AvailableFood_Table.Items.Contains(SelectedSearched))
                    throw new Exception("Searched food is not available in the selected date");

                AvailableFood_Table.SelectedItem = SelectedSearched;

                textBox.Text = "";
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }

        //   tab3     ///////////////////////////////////////
        private void Cart_Table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Cart_Table.SelectedItem == null)
            {
                //empty textblocks
                SelectedCartFood_Title.Text = "";
                quantity2.Value = 0;
                quantity2.IsEnabled = false;

                EditQuantity_Button.IsEnabled = false;
                RemoveFood_Button.IsEnabled = false;
            }
            else
            {
                OrderedFood SelectedFoodFromCart = (OrderedFood)Cart_Table.SelectedItem;

                SelectedCartFood_Title.Text = SelectedFoodFromCart.Title;
                quantity2.Value = SelectedFoodFromCart.Quantity;
                quantity2.Maximum = SelectedFoodFromCart.InMenu.TempQuantity;
                quantity2.IsEnabled = true;

                EditQuantity_Button.IsEnabled = true;
                RemoveFood_Button.IsEnabled = true;
            }
        }
        private void EditQuantity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Cart_Table.SelectedItem == null)
                    throw new Exception("Select item from cart");

                OrderedFood SelectedFoodFromCart = (OrderedFood)Cart_Table.SelectedItem;
                customer.AddToCart(SelectedFoodFromCart.InMenu, (int)quantity2.Value);

                //datagrid
                Cart_Table.SelectedItem = null;
                Cart_Table.Items.Clear();
                foreach (var orderedFood in customer.Cart)
                    Cart_Table.Items.Add(orderedFood);
                Cart_Table_SelectionChanged(null, null);

                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Cart was successfully edited";
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }
        private void RemoveFood_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Cart_Table.SelectedItem == null)
                    throw new Exception("Select item from cart");

                OrderedFood SelectedFoodFromCart = (OrderedFood)Cart_Table.SelectedItem;
                customer.RemoveFromCart(SelectedFoodFromCart);

                //datagrid
                Cart_Table.SelectedItem = null;
                Cart_Table.Items.Clear();
                foreach (var orderedFood in customer.Cart)
                    Cart_Table.Items.Add(orderedFood);
                Cart_Table_SelectionChanged(null, null);

                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Cart was successfully edited";
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }
        private void CompleteOrder_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (customer.Cart.Count == 0)
                    throw new Exception("Cart is empty");

                MessageBoxResult result = System.Windows.MessageBox.Show("You are about to complete your order.\nDo you wish to continue?", "", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (result == MessageBoxResult.No)
                    return;

                if (DatePicker.SelectedDate == null)
                    throw new Exception("Select date from AddToCart page");

                InOrderingProcess = true;
                Signature_Canvas.Children.Clear();
                CreditCard.Text = "";
                DiscountCode_Textbox.Text = "";
                Desk.SelectedIndex = 4;
                Desk.Visibility = Visibility.Visible;
                SignatureGrid.Visibility = Visibility.Visible;
                ReciptGrid.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }

        //   tab4     ///////////////////////////////////////
        private void Orders_Table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Orders_Table.SelectedItem == null)
            {
                OrderPrint.Text = "";
                Signature_Image.Source = null;
                CancelOrder_Button.IsEnabled = false;
            }
            else
            {
                Order SelectedOrder = (Order)Orders_Table.SelectedItem;
                OrderPrint.Text = SelectedOrder.Print(false);
                Signature_Image.Source = new ImageSourceConverter().ConvertFromString(SelectedOrder.Signature_Path) as ImageSource;
                CancelOrder_Button.IsEnabled = true;
            }
        }
        private void CancelOrder_Button_Click(object sender, RoutedEventArgs e)
        {
            if (Orders_Table.SelectedItem == null)
                throw new Exception("Select an order");

            Order SelectedOrder = (Order)Orders_Table.SelectedItem;

            if (SelectedOrder.OrderDueDate.Date < DateTime.Today.Date || (SelectedOrder.PaymentMethod == PaymentMethod.Cash_WhenDelivered && SelectedOrder.IsPaid))
                throw new Exception("Order DueDate Has Passed and the order is done");

            //confirming order cancel
            MessageBoxResult result = System.Windows.MessageBox.Show($"You are about to cancel an order (OrderID:{SelectedOrder.OrderID})\nAre you sure?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (result == MessageBoxResult.No)
                return;

            SelectedOrder.Cancel();

            //returning money
            string messageBoxText = "";
            if (SelectedOrder.PaymentMethod == PaymentMethod.Online && SelectedOrder.IsPaid)
                messageBoxText = $"Order canceled.\n{SelectedOrder.Total_AfterTaxDiscount * 9 / 10} has returned to your account.\n{SelectedOrder.Total_AfterTaxDiscount / 10} is taken for taxes.";
            else if(!SelectedOrder.IsPaid)
                messageBoxText = $"Order canceled.\nPlease pay {SelectedOrder.Total_AfterTaxDiscount / 10} online for taxes.";
            
            System.Windows.MessageBox.Show(messageBoxText, "", MessageBoxButton.OK, MessageBoxImage.Information);

            Customer.Save();
            Restaurant.Save();

            //reload page
            Orders_Table.SelectedItem = null;
            Orders_Table.Items.Clear();
            foreach (var Order in customer.Orders)
                Orders_Table.Items.Add(Order);

            OrderPrint.Text = "";
            Signature_Image.Source = null;
            CancelOrder_Button.IsEnabled = false;
        }

        //   tab5     ///////////////////////////////////////
        private void CreditCard_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (CreditCard.Text.Length % 5 == 4)
            {
                CreditCard.Text += " ";
                CreditCard.SelectionStart = CreditCard.Text.Length;
            }
        }
        Point currentPoint = new Point();
        private void Canvas_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
                currentPoint = e.GetPosition(Signature_Canvas);
        }
        private void Canvas_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Line line = new Line();

                line.Stroke = SystemColors.WindowFrameBrush;
                line.X1 = currentPoint.X;
                line.Y1 = currentPoint.Y;
                line.X2 = e.GetPosition(Signature_Canvas).X;
                line.Y2 = e.GetPosition(Signature_Canvas).Y;

                currentPoint = e.GetPosition(Signature_Canvas);

                Signature_Canvas.Children.Add(line);
            }
        }
        private void Reset_Button_Click(object sender, RoutedEventArgs e)
        {
            Signature_Canvas.Children.Clear();
        }
        private void Cancel_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = System.Windows.MessageBox.Show("You are about to cancel.\nAre you sure?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (result == MessageBoxResult.No)
                return;

            InOrderingProcess = false;
            customer.EmptyCart();
            Desk.SelectedIndex = 1;
            Desk.Visibility = Visibility.Collapsed;
        }
        private void Next_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CreditCard.Text == "")
                    throw new Exception("Enter credit card number");

                DiscountCode EnteredCode = DiscountCode_Textbox.Text.ExtractDiscountCode(customer);

                MessageBoxResult result = System.Windows.MessageBox.Show("You are about to create an order.\nAre you sure?", "", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (result == MessageBoxResult.No)
                    return;

                SignatureGrid.Visibility = Visibility.Collapsed;
                ReciptGrid.Visibility = Visibility.Visible;

                if (DatePicker.SelectedDate == null)
                {
                    Cancel_Button_Click(null, null);
                    throw new Exception("Pick a Date First");
                }
                var SelectedDate = DatePicker.SelectedDate ?? DateTime.Today;

                Order NewOrder = new Order(customer, SelectedDate, EnteredCode, Signature_Canvas);
                Recipt_Preview.Text = NewOrder.Print(false);
                Signature_Preview.Source = new ImageSourceConverter().ConvertFromString(NewOrder.Signature_Path) as ImageSource;
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }

        //   tab6     ///////////////////////////////////////
        private void Cancel_Button_Click_2(object sender, RoutedEventArgs e)
        {
            Cancel_Button_Click(null, null);
            customer.Orders.Last().Cancel();
        }
        private void Payment_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = System.Windows.MessageBox.Show($"Do You wish to pay {customer.Orders.Last().Total_AfterTaxDiscount} online?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.Cancel)
                return;
            else if (result == MessageBoxResult.Yes)
            {
                System.Windows.MessageBox.Show($"{customer.Orders.Last().Total_AfterTaxDiscount} has been taken from your account\nThanks For Your Purchase", "", MessageBoxButton.OK, MessageBoxImage.Information);
                customer.Orders.Last().IsPaid = true;
                customer.Orders.Last().PaymentMethod = PaymentMethod.Online;
            }
            else if (result == MessageBoxResult.No)
            {
                System.Windows.MessageBox.Show($"You chose to pay when the order is delivered\nThanks For Your Purchase", "", MessageBoxButton.OK, MessageBoxImage.Information);
                customer.Orders.Last().PaymentMethod = PaymentMethod.Cash_WhenDelivered;
            }

            InOrderingProcess = false;
            customer.EmptyCart();
            DatePicker.SelectedDate = null;
            MessageBox.Text = "Select From Menu";
            Menu3.BorderBrush = Brushes.DeepSkyBlue;
            ReciptGrid.Visibility = Visibility.Collapsed;
            Desk.SelectedIndex = 1;
            Desk.Visibility = Visibility.Collapsed;
            customer.CodeGenerator();
            Restaurant.Save();
        }
    }
}