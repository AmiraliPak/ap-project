﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Linq;
using System.Globalization;
using System.Security.Cryptography.Xml;

namespace AP_Final_Project
{
    /// <summary>
    /// Interaction logic for ManagerMenu.xaml
    /// </summary>
    public partial class ManagerMenu : Window
    {
        Manager manager;
        public ManagerMenu(Manager manager)
        {
            InitializeComponent();
            this.manager = manager;
            UserInfo.Text = manager.UserName;
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var DefaultMessageColor = Brushes.AliceBlue;
            //when its off
            if (((Button)sender).BorderBrush != Brushes.Yellow)
            {
                //turn off other buttons
                Menu1.BorderBrush = Brushes.DeepSkyBlue;
                Menu2.BorderBrush = Brushes.DeepSkyBlue;
                Menu3.BorderBrush = Brushes.DeepSkyBlue;
                Menu4.BorderBrush = Brushes.DeepSkyBlue;

                //turn on this button
                ((Button)sender).BorderBrush = Brushes.Yellow;
                //set textblocks
                if (((Button)sender).Name == Menu1.Name)
                {
                    MessageBox.Foreground = DefaultMessageColor;
                    MessageBox.Text = "Restaurant info and profits are shown above";
                    Desk.SelectedIndex = 0;
                    //profile and menu images
                    Profile_Image.Source = new ImageSourceConverter().ConvertFromString(manager.ProfilePicture_Path) as ImageSource;
                    if(Restaurant.restaurant.MenuImage_Path!=null)
                        Menu_Image.Source = new ImageSourceConverter().ConvertFromString(Restaurant.restaurant.MenuImage_Path) as ImageSource;
                    //preveous infos
                    Edit_Name.Text = manager.Name;
                    Edit_LastName.Text = manager.LastName;
                    Edit_Email.Text = manager.Email;
                    Edit_PhoneNumber.Text = manager.PhoneNumber;
                    Edit_District.Text = Restaurant.restaurant.District;
                    Edit_Kind.Text = Restaurant.restaurant.Kind;
                    Edit_Address.Text = Restaurant.restaurant.Address;
                    //today profit

                    var TodayPaidOrders =
                        Restaurant.restaurant.Orders
                        .Where(x => (x.OrderDateTime.Date == DateTime.Today.Date && x.PaymentMethod==PaymentMethod.Online && x.IsPaid)||(x.OrderDueDate.Date == DateTime.Today.Date && x.PaymentMethod == PaymentMethod.Cash_WhenDelivered));

                    var TodayTotalProfit =
                        TodayPaidOrders
                        .Sum(x => (x.TotalSellingPrice - x.TotalCostPrice));

                    var TodayTotalSellings =
                        TodayPaidOrders
                        .Sum(x => x.TotalSellingPrice);

                    MessageBox.Foreground = Brushes.YellowGreen;
                    MessageBox.Text = $"Today's sellings: {TodayTotalSellings}  Today's profit: {TodayTotalProfit}";
                }
                else if (((Button)sender).Name == Menu2.Name)
                {
                    MessageBox.Foreground = DefaultMessageColor;
                    MessageBox.Text = "Menu is shown. Select a food to set quantity.";
                    Desk.SelectedIndex = 1;

                    //DatePicker setup
                    DatePicker.SelectedDate = null;
                    DatePicker.DisplayDateStart = DateTime.Today.Date;
                    DatePicker.DisplayDateEnd = DateTime.Today.Date.AddMonths(1);
                    //empty datagrid
                    FoodQuantity_Table.SelectedItem = null;
                    FoodQuantity_Table.Items.Clear();
                    FoodQuantity_Table.IsEnabled = false;
                    //empty textblocks
                    SelectedFood_Title.Text = "";
                    quantity.Value = 0;
                    quantity.IsEnabled = false;
                    
                    ApplyFoodQuantity.IsEnabled = false;
                }
                else if (((Button)sender).Name == Menu3.Name)
                {
                    MessageBox.Foreground = DefaultMessageColor;
                    MessageBox.Text = "Enter the required field to add a food to menu";
                    Desk.SelectedIndex = 2;

                    FoodKind_Combo.ItemsSource = Enum.GetValues(typeof (FoodKind));

                    EditMenu_Table.SelectedItem = null;
                    EditMenu_Table.Items.Clear();
                    foreach (var food in Restaurant.restaurant.Menu)
                        EditMenu_Table.Items.Add(food);

                }
                else if (((Button)sender).Name == Menu4.Name)
                {
                    MessageBox.Foreground = DefaultMessageColor;
                    MessageBox.Text = "Select an order to show details";
                    Desk.SelectedIndex = 3;

                    PaidOrders_Table.SelectedItem = null;
                    PaidOrders_Table.Items.Clear();
                    foreach (var PaidOrder in Restaurant.restaurant.Orders.Where(x => x.IsPaid && !x.IsCanceledByManager))
                        PaidOrders_Table.Items.Add(PaidOrder);

                    UnPaidOrders_Table.SelectedItem = null;
                    UnPaidOrders_Table.Items.Clear();
                    foreach (var UnPaidOrder in Restaurant.restaurant.Orders.Where(x => !(x.IsPaid) && !x.IsCanceledByManager))
                        UnPaidOrders_Table.Items.Add(UnPaidOrder);

                    OrderPrint.Text = "";
                    Signature.Source = null;
                }
                Desk.Visibility = Visibility.Visible;
            }
            //when its on
            else
            {
                    MessageBox.Foreground = DefaultMessageColor;
                    MessageBox.Text = "Select From Menu";
                    ((Button)sender).BorderBrush = Brushes.DeepSkyBlue;
                    Desk.Visibility = Visibility.Collapsed;
            }
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Manager.Save();
            Restaurant.Save();
            Customer.Save();

            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        //    tab1   ///////////////////////////////////////
        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Edit_Name.Text != manager.Name)
                {
                    if (!Edit_Name.Text.IsValidName())
                        throw new Exception("Invalid Name");
                    manager.Name = Edit_Name.Text;
                }

                if (Edit_LastName.Text != manager.LastName)
                {
                    if (!Edit_LastName.Text.IsValidName())
                        throw new Exception("Invalid LastName");
                    manager.LastName = Edit_LastName.Text;
                }

                if (Edit_Email.Text != manager.Email)
                {
                    if (!Edit_Email.Text.IsEmail())
                        throw new Exception("Invalid Email");
                    manager.Email = Edit_Email.Text;
                }

                if (Edit_PhoneNumber.Text != manager.PhoneNumber && Edit_PhoneNumber.Text != "")
                    manager.PhoneNumber = Edit_PhoneNumber.Text;

                if (Edit_District.Text != Restaurant.restaurant.District && Edit_District.Text != "")
                    Restaurant.restaurant.District = Edit_District.Text;

                if (Edit_Kind.Text != Restaurant.restaurant.Kind && Edit_Kind.Text != "")
                    Restaurant.restaurant.Kind = Edit_Kind.Text;

                if (Edit_Address.Text != Restaurant.restaurant.Address && Edit_Address.Text != "")
                    Restaurant.restaurant.Address = Edit_Address.Text;

                Manager.Save();
                Restaurant.Save();

                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Fields Successfully Changed";
            }
            catch(Exception ex) { MessageBox.Foreground=Brushes.Red; MessageBox.Text = ex.Message; }

        }
        private void SetProfilePicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.jpg;*.png)|*.jpg;*.png";
            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                manager.ProfilePicture_Path = openFileDialog.FileName;
                Profile_Image.Source = new ImageSourceConverter().ConvertFromString(manager.ProfilePicture_Path) as ImageSource;
                Manager.Save();
                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Profile Image Successfully Changed";
            }
        }
        private void SetMenuPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.jpg;*.png)|*.jpg;*.png";
            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                Restaurant.restaurant.MenuImage_Path = openFileDialog.FileName;
                Menu_Image.Source = new ImageSourceConverter().ConvertFromString(Restaurant.restaurant.MenuImage_Path) as ImageSource;
                Restaurant.Save();
                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Menu Image Successfully Changed";
            }
        }
        
        //   tab3     ///////////////////////////////////////
        private void ApplyFood_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (NewFood_Title.Text == "")
                    throw new Exception("Enter Food Title");
                if (FoodKind_Combo.SelectedItem == null)
                    throw new Exception("Select Food Kind");
                if (NewFood_CostPrice.Text == "")
                    throw new Exception("Enter Food CostPrice");
                if (NewFood_Ingredients.Text == "")
                    throw new Exception("Enter Food Ingredients");

                long NewCostPrice = long.Parse(NewFood_CostPrice.Text);

                if (EditMenu_Table.SelectedItem == null)
                {
                    //add new food to menu
                    Restaurant.restaurant.AddToMenu((FoodKind)FoodKind_Combo.SelectedItem, NewFood_Title.Text, NewFood_Ingredients.Text, NewCostPrice, NewFood_ExtraInfo.Text, new ImageSourceConverter().ConvertToString(Food_Image.Source));
                    MessageBox.Foreground = Brushes.YellowGreen;
                    MessageBox.Text = "New Food Successfully Added to Menu";
                }
                else
                {
                    //edit current food
                    Food SelectedFoodFromMenu = (Food)EditMenu_Table.SelectedItem;

                    SelectedFoodFromMenu.Title = NewFood_Title.Text;
                    SelectedFoodFromMenu.foodKind = (FoodKind)FoodKind_Combo.SelectedItem;
                    SelectedFoodFromMenu.CostPrice = NewCostPrice;
                    SelectedFoodFromMenu.Ingredients = NewFood_Ingredients.Text;
                    SelectedFoodFromMenu.ExtraInfo = NewFood_ExtraInfo.Text;
                    SelectedFoodFromMenu.Image_Path = new ImageSourceConverter().ConvertToString(Food_Image.Source);

                    Restaurant.Save();

                    MessageBox.Foreground = Brushes.YellowGreen;
                    MessageBox.Text = "Food Was Successfully Edited";
                }

                NewFood_Click(null, null);
            }
            catch(Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }
        private void FoodKind_Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(FoodKind_Combo.SelectedItem != null)
            {
                var PizzaImageSource = new ImageSourceConverter().ConvertFromString(Food.Pizza_DefaultImage) as ImageSource;
                var SandwichImageSource = new ImageSourceConverter().ConvertFromString(Food.Sandwich_DefaultImage) as ImageSource;
                var AppetizerImageSource = new ImageSourceConverter().ConvertFromString(Food.Appetizer_DefaultImage) as ImageSource;
                var BeverageImageSource = new ImageSourceConverter().ConvertFromString(Food.Beverage_DefaultImage) as ImageSource;

                switch ((FoodKind)FoodKind_Combo.SelectedItem)
                {
                    case FoodKind.Pizza:
                        Food_Image.Source = PizzaImageSource;
                        break;
                    case FoodKind.Sandwich:
                        Food_Image.Source = SandwichImageSource;
                        break;
                    case FoodKind.Appetizer:
                        Food_Image.Source = AppetizerImageSource;
                        break;
                    case FoodKind.Beverage:
                        Food_Image.Source = BeverageImageSource;
                        break;
                }
            }
        }
        private void SetFoodPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.jpg;*.png)|*.jpg;*.png";
            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                Food_Image.Source = new ImageSourceConverter().ConvertFromString(openFileDialog.FileName) as ImageSource;
                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Food Image Successfully Changed";
            }
        }
        private void EditMenu_Table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (EditMenu_Table.SelectedItem == null)
            {
                NewFood_Click(null, null);
                return;
            }
            Food SelectedFoodFromMenu = (Food)EditMenu_Table.SelectedItem;

            FoodKind_Combo.SelectedItem = SelectedFoodFromMenu.foodKind;
            //selected food image
            Food_Image.Source = new ImageSourceConverter().ConvertFromString(SelectedFoodFromMenu.Image_Path) as ImageSource;
            //selected food info
            NewFood_Title.Text = SelectedFoodFromMenu.Title;


            NewFood_CostPrice.Text = SelectedFoodFromMenu.CostPrice.ToString();
            NewFood_Ingredients.Text = SelectedFoodFromMenu.Ingredients;
            NewFood_ExtraInfo.Text = SelectedFoodFromMenu.ExtraInfo;
        }
        private void NewFood_Click(object sender, RoutedEventArgs e)
        {
            EditMenu_Table.SelectedItem = null;

            Food_Image.Source = null;
            NewFood_Title.Text = "";
            FoodKind_Combo.SelectedItem = null;
            NewFood_CostPrice.Text = "";
            NewFood_Ingredients.Text = "";
            NewFood_ExtraInfo.Text = "";

            EditMenu_Table.SelectedItem = null;
            EditMenu_Table.Items.Clear();
            foreach (var food in Restaurant.restaurant.Menu)
                EditMenu_Table.Items.Add(food);

            MessageBox.Foreground = Brushes.AliceBlue;
            MessageBox.Text = "Enter required fields and click on apply";
        }
        private void EditMenu_Table_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            return;
        }

        //   tab2     ///////////////////////////////////////
        private void ApplyFoodQuantity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DatePicker.SelectedDate == null)
                    throw new Exception("Select a date");
                if (FoodQuantity_Table.SelectedItem == null)
                    throw new Exception("Select a food from menu");

                Food SelectedFoodFromMenu = (Food)FoodQuantity_Table.SelectedItem;
                var SelectedDate = DatePicker.SelectedDate ?? DateTime.Today.Date;
                var AvailableFood =
                    Restaurant.restaurant.Menu
                    .Where(x => x.DateQuantity.ContainsKey(SelectedDate) && x.DateQuantity[SelectedDate] > 0);

                SelectedFoodFromMenu.DateQuantity[SelectedDate] = (int)quantity.Value;
                if (AvailableFood.Count() > 7)
                {
                    SelectedFoodFromMenu.DateQuantity[SelectedDate] = 0;
                    throw new Exception("Food variety more than 7 in a day is not allowed");
                }
                Restaurant.Save();

                MessageBox.Foreground = Brushes.YellowGreen;
                MessageBox.Text = "Food Quantity Was Successfully Set";

                DatePicker_SelectedDateChanged(null, null);
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }
        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DatePicker.SelectedDate == null)
                return;
            var SelectedDate = DatePicker.SelectedDate ?? DateTime.Today.Date;

            FoodQuantity_Table.SelectedItem = null;//func
            FoodQuantity_Table.Items.Clear();
            foreach (var food in Restaurant.restaurant.Menu)
            {
                if (food.DateQuantity.ContainsKey(SelectedDate))
                    food.TempQuantity = food.DateQuantity[SelectedDate];
                else
                    food.TempQuantity = 0;

                FoodQuantity_Table.Items.Add(food);
            }

            FoodQuantity_Table_SelectionChanged(null, null);
            FoodQuantity_Table.IsEnabled = true;

            MessageBox.Foreground = Brushes.AliceBlue;
            MessageBox.Text = "Menu is shown. Select a food to set quantity.";
        }
        private void FoodQuantity_Table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FoodQuantity_Table.SelectedItem == null)
            {
                //empty textblocks
                SelectedFood_Title.Text = "";
                quantity.Value = 0;
                quantity.IsEnabled = false;

                ApplyFoodQuantity.IsEnabled = false;
            }
            else
            {
                Food SelectedFoodFromMenu = (Food)FoodQuantity_Table.SelectedItem;

                SelectedFood_Title.Text = SelectedFoodFromMenu.Title;
                quantity.Value = SelectedFoodFromMenu.TempQuantity;
                quantity.IsEnabled = true;

                ApplyFoodQuantity.IsEnabled = true;
            }
        }

        //   tab4     ///////////////////////////////////////
        private void Orders_Table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PaidOrders_Table.SelectedItem == null && UnPaidOrders_Table.SelectedItem == null)
            {
                PaidOrders_Table.SelectedItem = null;
                UnPaidOrders_Table.SelectedItem = null;
                OrderPrint.Text = "";
                Signature.Source = null;
            }
            else if (((DataGrid)sender).SelectedItem == null)
                return;
            else
            {
                if(((DataGrid)sender)==PaidOrders_Table)
                    UnPaidOrders_Table.SelectedItem = null;
                else
                    PaidOrders_Table.SelectedItem = null;

                Order SelectedOrder = (Order)((DataGrid)sender).SelectedItem;
                OrderPrint.Text = SelectedOrder.Print(true);
                Signature.Source = new ImageSourceConverter().ConvertFromString(SelectedOrder.Signature_Path) as ImageSource;
            }
        }
        private void CancelOrder_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PaidOrders_Table.SelectedItem == null && UnPaidOrders_Table.SelectedItem == null)
                    throw new Exception("Select an order");

                Order SelectedOrder = null;
                if (UnPaidOrders_Table.SelectedItem == null)
                    SelectedOrder = (Order)(PaidOrders_Table).SelectedItem;
                else if (PaidOrders_Table.SelectedItem == null)
                    SelectedOrder = (Order)(UnPaidOrders_Table).SelectedItem;

                if (SelectedOrder.OrderDueDate.Date < DateTime.Today.Date)
                    throw new Exception("Order is already completed");

                MessageBoxResult result = System.Windows.MessageBox.Show("You are about to cancel an order.\nDo you wish to continue?", "", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                    return;

                SelectedOrder.IsCanceledByManager = true;
                Restaurant.Save();
                Customer.Save();

                //reload page
                PaidOrders_Table.SelectedItem = null;
                PaidOrders_Table.Items.Clear();
                foreach (var PaidOrder in Restaurant.restaurant.Orders.Where(x => x.IsPaid && !x.IsCanceledByManager))
                    PaidOrders_Table.Items.Add(PaidOrder);

                UnPaidOrders_Table.SelectedItem = null;
                UnPaidOrders_Table.Items.Clear();
                foreach (var UnPaidOrder in Restaurant.restaurant.Orders.Where(x => !(x.IsPaid) && !x.IsCanceledByManager))
                    UnPaidOrders_Table.Items.Add(UnPaidOrder);

                OrderPrint.Text = "";
                Signature.Source = null;
            }
            catch (Exception ex) { MessageBox.Foreground = Brushes.Red; MessageBox.Text = ex.Message; }
        }
    }
}