﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AP_Final_Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Restaurant.restaurant = Extensions.Deserialize<Restaurant>("Restaurant.bin");
            Customer.Customers = Extensions.Deserialize<List<Customer>>("Customers.bin");
            Manager.Managers = Extensions.Deserialize<List<Manager>>("Managers.bin");

            Restaurant.restaurant.Orders.Clear();
            foreach(var customer in Customer.Customers)
            {
                foreach (var order in customer.Orders)
                    Restaurant.restaurant.Orders.Add(order);
            }
            Restaurant.restaurant.Orders = Restaurant.restaurant.Orders.OrderBy(x => x.OrderID).ToList();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //this function is for the manager and customer buttons
            //when its off
            if (((Button)sender).BorderBrush != Brushes.DeepSkyBlue)
            {
                Error_Border.Visibility = Visibility.Hidden;
                //turn off other buttons
                Customer_Button.BorderBrush = Brushes.Gray;
                Customer_Button.BorderThickness = new Thickness(1);
                Manager_Button.BorderBrush = Brushes.Gray;
                Manager_Button.BorderThickness = new Thickness(1);
                //turn on this button
                ((Button)sender).BorderBrush = Brushes.DeepSkyBlue;
                ((Button)sender).BorderThickness = new Thickness(5);
                //set textblocks
                Tabs.SelectedIndex = 0;
                Tabs.Visibility = Visibility.Visible;
                Login_Username.Text = "";
                Login_Password.Password = "";
                if (((Button)sender).Name == Manager_Button.Name)
                {
                    SignUp_Button.Visibility = Visibility.Hidden;
                    Username_TBlock.Text = "UserName:";
                    Login_Username.Foreground = Brushes.Black;
                }
                else if (((Button)sender).Name == Customer_Button.Name)
                {
                    SignUp_Button.Visibility = Visibility.Visible;
                    Username_TBlock.Text = "Email:";
                    Login_Username.Text = "Or PhoneNumber";
                    Login_Username.Foreground = Brushes.Gray;
                }
            }
            //when its on
            else
            {
                ((Button)sender).BorderBrush = Brushes.Gray;
                ((Button)sender).BorderThickness = new Thickness(1);
                Tabs.Visibility = Visibility.Hidden;
            }
        }

        private void SignUp_Button_Click(object sender, RoutedEventArgs e)
        {
            Tabs.SelectedIndex = 1;

            SU_Email.Text = "";
            SU_Name.Text = "";
            SU_LastName.Text = "";
            SU_PhoneNumber.Text = "";
            SU_CitizenID.Text = "";
            SU_Password.Password = "";
            SU_ReEnterPassword.Password = "";

            SUError_Border.Visibility = Visibility.Hidden;
            Tabs.Visibility = Visibility.Visible;
        }

        private void CanselSU_Click(object sender, RoutedEventArgs e)
        {
            Tabs.SelectedIndex = 0;
            Tabs.Visibility = Visibility.Visible;
        }

        private void CompleteSU_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!SU_Name.Text.IsValidName())
                    throw new Exception("Invalid Name");
                if (!SU_LastName.Text.IsValidName())
                    throw new Exception("Invalid LastName");
                if (!SU_PhoneNumber.Text.IsPhoneNumber())
                    throw new Exception("Invalid PhoneNumber");
                if (!SU_Email.Text.IsEmail())
                    throw new Exception("Invalid Email");
                if (!SU_CitizenID.Text.IsCitizenID())
                    throw new Exception("Invalid CitizenID");
                if (SU_Address.Text == "")
                    throw new Exception("Enter Address");
                if (!SU_Password.Password.IsValidPassword())
                    throw new Exception("Invalid Password");
                if (SU_ReEnterPassword.Password != SU_Password.Password)
                    throw new Exception("Different Passwords");

                if (Customer.Customers.Select(x => x.Email).Contains(SU_Email.Text))
                    throw new Exception("Email already signed up");
                if (Customer.Customers.Select(x => x.PhoneNumber).Contains(SU_PhoneNumber.Text.FormatPhoneNumber()))
                    throw new Exception("PhoneNumber already signed up");
                if (Customer.Customers.Select(x => x.CitizenID).Contains(SU_CitizenID.Text))
                    throw new Exception("CitizenID already signed up");

                new Customer(SU_Email.Text, SU_Password.Password, SU_CitizenID.Text, SU_Address.Text, SU_Name.Text, SU_LastName.Text, SU_PhoneNumber.Text);
                Tabs.SelectedIndex = 0;
                Tabs.Visibility = Visibility.Visible;

                Error_Border.Visibility = Visibility.Visible;
                Error_TB.Text = "Account SignedUp";
            }
            catch(Exception ex) { SUError_Border.Visibility = Visibility.Visible; SUError_TB.Text = ex.Message; }
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Error_Border.Visibility = Visibility.Hidden;

                if (Customer_Button.BorderBrush == Brushes.DeepSkyBlue)
                    Customer.Login(Login_Username.Text, Login_Password.Password,this);
                else if(Manager_Button.BorderBrush == Brushes.DeepSkyBlue)
                    Manager.Login(Login_Username.Text, Login_Password.Password, this);
            }
            catch (Exception ex) { Error_Border.Visibility = Visibility.Visible; Error_TB.Text = ex.Message; }
        }

        private void Login_Username_IsKeyboardFocusedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(Customer_Button.BorderBrush == Brushes.DeepSkyBlue)
            {
                if (Login_Username.IsKeyboardFocused && Login_Username.Text == "Or PhoneNumber")
                {
                    Login_Username.Text = "";
                    Login_Username.Foreground = Brushes.Black;
                }
                else if (Login_Username.Text == "")
                {
                    Login_Username.Text = "Or PhoneNumber";
                    Login_Username.Foreground = Brushes.Gray;
                }
            }
        }
    }
}
