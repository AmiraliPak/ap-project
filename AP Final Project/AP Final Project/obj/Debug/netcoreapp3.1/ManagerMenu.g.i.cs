﻿#pragma checksum "..\..\..\ManagerMenu.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0339598A1CF9B0E0D30017E3319FC2E818A1BA81"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AP_Final_Project;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AP_Final_Project {
    
    
    /// <summary>
    /// ManagerMenu
    /// </summary>
    public partial class ManagerMenu : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock UserInfo;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Menu1;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Menu2;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Menu3;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Menu4;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MessageBox;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl Desk;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Edit_Name;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Edit_LastName;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Edit_Email;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Edit_PhoneNumber;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Edit_District;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Edit_Kind;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Edit_Address;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Profile_Image;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Menu_Image;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid FoodQuantity_Table;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker DatePicker;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SelectedFood_Title;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider quantity;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SelectedFood_Quantity;
        
        #line default
        #line hidden
        
        
        #line 183 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ApplyFoodQuantity;
        
        #line default
        #line hidden
        
        
        #line 209 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NewFood_Title;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox FoodKind_Combo;
        
        #line default
        #line hidden
        
        
        #line 211 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NewFood_CostPrice;
        
        #line default
        #line hidden
        
        
        #line 212 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NewFood_Ingredients;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NewFood_ExtraInfo;
        
        #line default
        #line hidden
        
        
        #line 218 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Food_Image;
        
        #line default
        #line hidden
        
        
        #line 221 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid EditMenu_Table;
        
        #line default
        #line hidden
        
        
        #line 245 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid PaidOrders_Table;
        
        #line default
        #line hidden
        
        
        #line 253 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid UnPaidOrders_Table;
        
        #line default
        #line hidden
        
        
        #line 261 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox OrderPrint;
        
        #line default
        #line hidden
        
        
        #line 262 "..\..\..\ManagerMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Signature;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AP Final Project;component/managermenu.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\ManagerMenu.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.UserInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.Menu1 = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\..\ManagerMenu.xaml"
            this.Menu1.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Menu2 = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\ManagerMenu.xaml"
            this.Menu2.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Menu3 = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\ManagerMenu.xaml"
            this.Menu3.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.Menu4 = ((System.Windows.Controls.Button)(target));
            
            #line 78 "..\..\..\ManagerMenu.xaml"
            this.Menu4.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 89 "..\..\..\ManagerMenu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Exit_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.MessageBox = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.Desk = ((System.Windows.Controls.TabControl)(target));
            return;
            case 9:
            this.Edit_Name = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.Edit_LastName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.Edit_Email = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.Edit_PhoneNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.Edit_District = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.Edit_Kind = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.Edit_Address = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            
            #line 144 "..\..\..\ManagerMenu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Edit_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.Profile_Image = ((System.Windows.Controls.Image)(target));
            return;
            case 18:
            
            #line 147 "..\..\..\ManagerMenu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.SetProfilePicture_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.Menu_Image = ((System.Windows.Controls.Image)(target));
            return;
            case 20:
            
            #line 150 "..\..\..\ManagerMenu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.SetMenuPicture_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.FoodQuantity_Table = ((System.Windows.Controls.DataGrid)(target));
            
            #line 165 "..\..\..\ManagerMenu.xaml"
            this.FoodQuantity_Table.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.FoodQuantity_Table_SelectionChanged);
            
            #line default
            #line hidden
            
            #line 165 "..\..\..\ManagerMenu.xaml"
            this.FoodQuantity_Table.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.EditMenu_Table_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 22:
            this.DatePicker = ((System.Windows.Controls.DatePicker)(target));
            
            #line 172 "..\..\..\ManagerMenu.xaml"
            this.DatePicker.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.DatePicker_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 23:
            this.SelectedFood_Title = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.quantity = ((System.Windows.Controls.Slider)(target));
            return;
            case 25:
            this.SelectedFood_Quantity = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            this.ApplyFoodQuantity = ((System.Windows.Controls.Button)(target));
            
            #line 183 "..\..\..\ManagerMenu.xaml"
            this.ApplyFoodQuantity.Click += new System.Windows.RoutedEventHandler(this.ApplyFoodQuantity_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            this.NewFood_Title = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.FoodKind_Combo = ((System.Windows.Controls.ComboBox)(target));
            
            #line 210 "..\..\..\ManagerMenu.xaml"
            this.FoodKind_Combo.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.FoodKind_Combo_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 29:
            this.NewFood_CostPrice = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.NewFood_Ingredients = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.NewFood_ExtraInfo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            
            #line 215 "..\..\..\ManagerMenu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ApplyFood_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            
            #line 216 "..\..\..\ManagerMenu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.NewFood_Click);
            
            #line default
            #line hidden
            return;
            case 34:
            this.Food_Image = ((System.Windows.Controls.Image)(target));
            return;
            case 35:
            
            #line 219 "..\..\..\ManagerMenu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.SetFoodPicture_Click);
            
            #line default
            #line hidden
            return;
            case 36:
            this.EditMenu_Table = ((System.Windows.Controls.DataGrid)(target));
            
            #line 222 "..\..\..\ManagerMenu.xaml"
            this.EditMenu_Table.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EditMenu_Table_SelectionChanged);
            
            #line default
            #line hidden
            
            #line 222 "..\..\..\ManagerMenu.xaml"
            this.EditMenu_Table.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.EditMenu_Table_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 37:
            this.PaidOrders_Table = ((System.Windows.Controls.DataGrid)(target));
            
            #line 246 "..\..\..\ManagerMenu.xaml"
            this.PaidOrders_Table.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Orders_Table_SelectionChanged);
            
            #line default
            #line hidden
            
            #line 246 "..\..\..\ManagerMenu.xaml"
            this.PaidOrders_Table.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.EditMenu_Table_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 38:
            this.UnPaidOrders_Table = ((System.Windows.Controls.DataGrid)(target));
            
            #line 254 "..\..\..\ManagerMenu.xaml"
            this.UnPaidOrders_Table.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Orders_Table_SelectionChanged);
            
            #line default
            #line hidden
            
            #line 254 "..\..\..\ManagerMenu.xaml"
            this.UnPaidOrders_Table.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.EditMenu_Table_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 39:
            this.OrderPrint = ((System.Windows.Controls.TextBox)(target));
            return;
            case 40:
            this.Signature = ((System.Windows.Controls.Image)(target));
            return;
            case 41:
            
            #line 263 "..\..\..\ManagerMenu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CancelOrder_Button_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

