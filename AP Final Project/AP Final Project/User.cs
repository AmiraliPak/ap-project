﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;
using System.Windows;

namespace AP_Final_Project
{
    //after every edit in info new serialization or after exiting to mainwindow
    [Serializable]
    public abstract class User
    {
        public User(string Name, string LastName,string Email,string CitizenID, string ProfilePicture_Path=null)
        {
            this.Name = Name;
            this.LastName = LastName;
            this.Email = Email;
            this.CitizenID = CitizenID;
            if (ProfilePicture_Path == null)
                ProfilePicture_Path = "E:/UNI/Term2/AP Project/ap-project/AP Final Project/AP Final Project/assets/DefaultUser.png";
            this.ProfilePicture_Path = ProfilePicture_Path;
        }

        public string Name{get; set;}

        public string LastName { get; set; }

        public string Email { get; set; }

        public string CitizenID { get; protected set; }

        public string ProfilePicture_Path { get; set; }
    }

    [Serializable]
    public class Customer : User
    {
        //database!
        public static List<Customer> Customers { set; get; } = new List<Customer>();
        public static void Save() { Extensions.Serialize("Customers.bin", Customer.Customers); }
        //customer information
        public Customer(string Email, string Password, string CitizenID, string Address, string Name, string LastName, string PhoneNumber, string ProfilePicture_Path=null):base(Name,LastName,Email,CitizenID,ProfilePicture_Path)
        {
            this.Password = Password;
            this.Address = Address;
            this.PhoneNumber = PhoneNumber.FormatPhoneNumber();

            Orders = new List<Order>();
            Cart = new List<OrderedFood>();

            Customers.Add(this);

            Extensions.Serialize("Customers.bin", Customers);
        }
        public string Address { get; private set; }
        public string PhoneNumber { get; set; }
        public static void Login(string EmailOrPhoneNum, string Password,MainWindow mainWindow)
        {
            EmailOrPhoneNum = EmailOrPhoneNum.FormatPhoneNumber();
            var FoundCustomer = Customers.Find(x => x.Email == EmailOrPhoneNum || x.PhoneNumber == EmailOrPhoneNum);

            if (FoundCustomer == null)
                throw new Exception("Account not found");
            if (FoundCustomer.Password != Password)
                throw new Exception("Wrong Password");
            
            var customerMenu = new CustomerMenu(FoundCustomer);
            customerMenu.Show();
            mainWindow.Close();
        }
        //password
        private string Password;
        public void ChangePassword(string NewPassword)
        {
            if (!NewPassword.IsValidPassword())
                throw new Exception("Password must contain at least one letter,number,special character");
            Password = NewPassword;
        }
        //orders
        public List<Order> Orders { set; get; } = new List<Order>();
        public int OrdersCount { get => Orders.Count; }
        //tax & discount
        public DiscountCode discountCode { private set; get; }
        public void CodeGenerator()
        {
            var TotalIn4Months =
                Orders
                .Where(x => (DateTime.Today.Date.Month - x.OrderDueDate.Date.Month) <= 4)
                .Sum(x => x.Total_AfterTaxDiscount);

            var Total = TotalIn4Months % 800001;

            int DiscountPercentage = 0;

            if (Total > 400000)
                DiscountPercentage = 15;
            else if (Total > 200000)
                DiscountPercentage = 10;
            else if (Total > 100000)
                DiscountPercentage = 5;
            else if (Total > 0 && TotalIn4Months > 800000)
                DiscountPercentage = 24;
            else
                return;

            if (discountCode!=null && discountCode.Percentage == DiscountPercentage)
                return;

            string Code = "";

            Code += Email.Substring(0, 5);

            Random random = new Random();
            var chars = "abcdefghijklmnopqrstuvwxyz0123456789@";
            var stringChars = new char[15];
            for (int i = 0; i < stringChars.Length; i++)
                stringChars[i] = chars[random.Next(chars.Length)];

            Code += new string(stringChars);

            Code += $"-{DiscountPercentage}";

            discountCode = new DiscountCode(Code, DiscountPercentage);
            discountCode.Show();
            Save();
        }
        public int TaxPercentage
        {
            get
            {
                if (OrdersCount <= 4)
                    return 9;
                else if (OrdersCount <= 8)
                    return 7;
                else if (OrdersCount <= 12)
                    return 5;
                else
                    return 0;
            }
        }
        public int DiscountPercentage
        {
            get
            {
                if (OrdersCount <= 4)
                    return 0;
                else if (OrdersCount <= 8)
                    return 5;
                else if (OrdersCount <= 12)
                    return 8;
                else
                    return 10;
            }
        }
        //cart
        public List<OrderedFood> Cart { get; private set; } = new List<OrderedFood>();
        public void AddToCart(Food Food, int Quantity)
        {
            Cart.Remove(Cart.Find(x => x.Title == Food.Title));
            Cart.Add(new OrderedFood(Food, Quantity));

            Save();
        }
        public void EmptyCart() 
        {
            if (Cart == null)
                Cart = new List<OrderedFood>();
            else
                Cart.Clear();

            Save();
        }
        public void RemoveFromCart(OrderedFood FoodToRemove) { Cart.Remove(FoodToRemove); Save(); }
    }

    [Serializable]
    public class Manager : User
    {
        public static void Save() { Extensions.Serialize("Managers.bin", Manager.Managers); }
        public static List<Manager> Managers { set; get; } = new List<Manager>();
        private int LoginCounts;

        public Manager(string UserName, string Name, string LastName,string Email,string CitizenID, string ProfilePicture_Path=null) : base(Name, LastName,Email,CitizenID, ProfilePicture_Path)
        {
            if (!UserName.Contains("admin"))
                throw new Exception("UserName must contain the word \"admin\"");
            this.UserName = UserName;

            LoginCounts = 0;

            Managers.Add(this);
            Extensions.Serialize("Managers.bin", Managers);
        }

        public string UserName { get; private set; }

        private string Password{ get => PasswordGenerator();}

        public string PhoneNumber { set => Restaurant.restaurant.PhoneNumber = value; get => Restaurant.restaurant.PhoneNumber; }

        public static void Login(string UserName, string Password, MainWindow mainWindow)
        {
            var FoundManager = Managers.Find(x => x.UserName == UserName);

            if (FoundManager == null)
                throw new Exception("Account not found");
            if (FoundManager.Password != Password)
                throw new Exception("Wrong Password");

            FoundManager.LoginCounts++;
            Extensions.Serialize("Managers.bin", Managers);

            var managerMenu = new ManagerMenu(FoundManager);
            managerMenu.Show();
            mainWindow.Close();
        }

        private string PasswordGenerator()
        {
            string tmp = "";
            for (int i = 0; i < LoginCounts % 10; i++)
                tmp += "1";

            int vowels = 0;
            foreach (char c in UserName.ToLower())
            {
                switch (c)
                {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                        vowels++;
                        break;
                }
            }
            for (int i = 0; i < vowels; i++)
                tmp += "0";

            return tmp;
        }
    }

    public static class Extensions
    {
        public static bool IsEmail(this string str)
        {
            // This Pattern is use to verify the email 
            string strRegex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);

            return re.IsMatch(str);
        }

        public static bool IsCitizenID(this string str)
        {
            var ID = str;
            //length check
            if (ID.Length != 10)
                return false;
            //convert to int
            int[] intID = new int[10];
            for (int i = 0; i < 10; i++)
            {
                if (ID[i] > '9' || ID[i] < '0')
                    return false;
                intID[i] = ID[i] - '0';
            }
            //chech if all the digits are the same
            bool AllTheSame = true;
            for (int i = 1; i < 10; i++)
            {
                if (intID[i] != intID[i - 1])
                    AllTheSame = false;
            }
            if (AllTheSame)
                return false;
            //check
            int a = intID[9];
            int b = 10 * intID[0] + 9 * intID[1] + 8 * intID[2] + 7 * intID[3] + 6 * intID[4] + 5 * intID[5] + 4 * intID[6] + 3 * intID[7] + 2 * intID[8];
            int c = b % 11;
            if (c == 0 && a == c)
                return true;
            if (c == 1 && a == 1)
                return true;
            if (c > 1 && a == Math.Abs(c - 11))
                return true;
            return false;
        }

        public static bool IsPhoneNumber(this string str)
        {
            Regex re = new Regex(@"^(00989|\+989|09|9)(\d{9})$");

            return re.IsMatch(str);
        }

        public static string FormatPhoneNumber(this string str)
        {
            if (str.IsPhoneNumber())
            {
                Regex re = new Regex(@"^(00989|\+989|09|9)(\d{9})$");
                return ("09" + re.Match(str).Groups[2].Value);
            }
            return str;
        }

        public static bool IsValidName (this string str)
        {
            Regex re = new Regex(@"^[a-zA-Z\s]+$");

            return re.IsMatch(str);
        }

        public static bool IsValidPassword(this string str)
        {
            Regex re = new Regex(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[^A-Za-z\d\s])[^\s]*$");

            return re.IsMatch(str);
        }

        public static DiscountCode ExtractDiscountCode(this string str,Customer customer)
        {
            DiscountCode EnteredCode = null;

            if (str != "")
            {
                if (customer.discountCode!=null && str == customer.discountCode.Code)
                {
                    if (customer.discountCode.Used)
                        throw new Exception("This Code is already used");
                    EnteredCode = customer.discountCode;
                }
                else if (str == DiscountCode.FirstCode.Code)
                {
                    if (customer.OrdersCount != 0)
                        throw new Exception("This Code is only valid for the first order");
                    EnteredCode = DiscountCode.FirstCode;
                }
                else if (str == DiscountCode.SecondCode.Code)
                {
                    if (customer.OrdersCount != 1)
                        throw new Exception("This Code is only valid for the second order");
                    EnteredCode = DiscountCode.SecondCode;
                }
                else
                    throw new Exception("Invalid Discount Code");

                if (EnteredCode.ExpiryDate < DateTime.Now)
                    throw new Exception("This Code is expired");
                return EnteredCode;
            }
            return null;
        }
        public static void Serialize<T>(string FilePath, T obj)
        {
            using (Stream stream = File.Open(FilePath, FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, obj);
            }
        }
        public static T Deserialize<T>(string FilePath) where T: new()
        {
            if (File.Exists(FilePath))
                using (Stream stream = File.Open(FilePath, FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    T obj = (T)bformatter.Deserialize(stream);
                    return obj;
                }
            else
                return new T();
        }
    }

    [Serializable]
    public class DiscountCode
    {
        public static DiscountCode FirstCode = new DiscountCode("first1order-5", 5);
        public static DiscountCode SecondCode = new DiscountCode("second2order-10", 10);
        public DiscountCode(string Code,int Percentage)
        {
            this.Code = Code;
            this.Percentage = Percentage;

            ExpiryDate = DateTime.Now.AddDays(Percentage);
            Used = false;
        }
        public string Code { get; private set; }
        public int Percentage { get; private set; }
        public DateTime ExpiryDate { get; private set; }
        public bool Used { get; set; }
        public void Show()
        {
            System.Windows.MessageBox.Show($"Congratulations\nYou got {Percentage}% Discount\nCode: {Code}\nThis code will expire in {Percentage} days", "", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        public override string ToString()
        {
            return Code;
        }
    }
}