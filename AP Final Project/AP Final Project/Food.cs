﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AP_Final_Project
{
    [Serializable]
    public class Food
    {
        public static string Pizza_DefaultImage = "E:/UNI/Term2/AP Project/ap-project/AP Final Project/AP Final Project/assets/Pizza.png";
        public static string Sandwich_DefaultImage = "E:/UNI/Term2/AP Project/ap-project/AP Final Project/AP Final Project/assets/Sandwich.png";
        public static string Appetizer_DefaultImage = "E:/UNI/Term2/AP Project/ap-project/AP Final Project/AP Final Project/assets/Appetizer.png";
        public static string Beverage_DefaultImage = "E:/UNI/Term2/AP Project/ap-project/AP Final Project/AP Final Project/assets/Beverage.png";
        public Food(FoodKind foodKind,string Title, string Ingredients, long CostPrice,string ExtraInfo=null,string Image_Path=null)
        {
            this.foodKind = foodKind;
            this.Title = Title;
            this.Ingredients = Ingredients;
            this.CostPrice = CostPrice;
            this.ExtraInfo = ExtraInfo;
            if (Image_Path == null)
            {
                switch (foodKind)
                {
                    case FoodKind.Pizza:
                        Image_Path = Pizza_DefaultImage;
                        break;
                    case FoodKind.Sandwich:
                        Image_Path = Sandwich_DefaultImage;
                        break;
                    case FoodKind.Appetizer:
                        Image_Path = Appetizer_DefaultImage;
                        break;
                    case FoodKind.Beverage:
                        Image_Path = Beverage_DefaultImage;
                        break; 
                }
            }
            this.Image_Path = Image_Path;
            DateQuantity = new SortedDictionary<DateTime, int>();
        }

        public string Image_Path { get; set; }

        public string Title { get; set; }

        public string Ingredients { get; set; }

        public FoodKind foodKind { get; set; }

        public string ExtraInfo { get; set; }

        public long CostPrice { get; set; }

        public int TempQuantity { get; set; }

        public long SellingPrice
        {
            get => CostPrice * (100 + InterestPercentage) / 100;
        }

        private static long InterestPercentage = 24;

        public SortedDictionary<DateTime, int> DateQuantity;
    }

    public enum FoodKind
    {
        Pizza,
        Sandwich,
        Appetizer,
        Beverage
    }

    [Serializable]
    public class OrderedFood
    {
        public OrderedFood(Food Food, int Quantity)
        {
            if (Quantity <= 0)
                throw new Exception("The Quantity cant be zero");

            this.Quantity = Quantity;
            Title = new string(Food.Title);
            CostPrice = Food.CostPrice;
            SellingPrice = Food.SellingPrice;

            InMenu = Food;
        }
        public Food InMenu { private set; get; }
        public string Title { get; private set; }

        public int Quantity { get; private set; }

        public long CostPrice { get; private set; }

        public long SellingPrice { get; private set; }
    }
}