﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AP_Final_Project;
using System;
using System.Collections.Generic;
using System.Text;

namespace AP_Final_Project.Tests
{
    [TestClass()]
    public class ExtensionsTests
    {
        [TestMethod()]
        public void IsEmailTest()
        {
            string str = "fksdnfknfl@hfdkfn.fdhnk";
            string str1 = "fksdnfknfl@hfdkfnfdhnk";
            string str2 = "fksdnfknflhfdkfn.fdhnk";
            string str3 = "fksdnfknflfdhnk";
            string str4 = "";
            string str5 = "@.";
            Assert.IsTrue(str.IsEmail());
            Assert.IsFalse(str1.IsEmail());
            Assert.IsFalse(str2.IsEmail());
            Assert.IsFalse(str3.IsEmail());
            Assert.IsFalse(str4.IsEmail());
            Assert.IsFalse(str5.IsEmail());
        }

        [TestMethod()]
        public void IsCitizenIDTest()
        {
            Assert.IsTrue("0023319331".IsCitizenID());
            Assert.IsFalse("231561513513".IsCitizenID());
            Assert.IsFalse("231561513".IsCitizenID());
            Assert.IsFalse("".IsCitizenID());
        }

        [TestMethod()]
        public void IsPhoneNumberTest()
        {
            Assert.IsTrue("09360136443".IsPhoneNumber());
            Assert.IsTrue("9360136443".IsPhoneNumber());
            Assert.IsTrue("00989360136443".IsPhoneNumber());
            Assert.IsTrue("+989360136443".IsPhoneNumber());
            Assert.IsFalse("0936013648".IsPhoneNumber());
            Assert.IsFalse("0936013644309360136443".IsPhoneNumber());
            Assert.IsFalse("37360136443".IsPhoneNumber());
            Assert.IsFalse("".IsPhoneNumber());
        }

        [TestMethod()]
        public void IsValidNameTest()
        {
            Assert.IsTrue("Amir ali".IsValidName());
            Assert.IsTrue("Amirali".IsValidName());
            Assert.IsFalse("Amir4ali".IsValidName());
            Assert.IsFalse("Amir*mali".IsValidName());
            Assert.IsFalse("".IsValidName());
        }

        [TestMethod()]
        public void IsValidPasswordTest()
        {
            Assert.IsTrue("Amirali4$".IsValidPassword());
            Assert.IsTrue("3m%irali".IsValidPassword());
            Assert.IsFalse("Amirali".IsValidPassword());
            Assert.IsFalse("Amir*mali".IsValidPassword());
            Assert.IsFalse("Amir0mali".IsValidPassword());
            Assert.IsFalse("".IsValidPassword());
        }

        [TestMethod()]
        public void FormatPhoneNumberTest()
        {
            string phone1 = "09360136443";
            string phone2 = "9360136443";
            string phone3 = "+989360136443";
            string phone4 = "00989360136443";

            string notphone = "ajdbks5@kdnflkd.com";

            string expected = "09360136443";
            Assert.AreEqual(expected,phone1.FormatPhoneNumber());
            Assert.AreEqual(expected,phone2.FormatPhoneNumber());
            Assert.AreEqual(expected,phone3.FormatPhoneNumber());
            Assert.AreEqual(expected,phone4.FormatPhoneNumber());
            
            Assert.AreEqual(notphone,notphone.FormatPhoneNumber());
        }
    }
}